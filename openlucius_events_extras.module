<?php
/**
 * @file
 * Code for the Openlucius Events feature.
 */

include_once 'openlucius_events_extras.features.inc';

/**
 * Implements hook_init().
 */
function openlucius_events_extras_init() {
  $node = menu_get_object();

  // Add css when current node is an event.
  if (isset($node->type) && !empty($node->type) && $node->type == 'ol_event') {
    drupal_add_css(drupal_get_path('module', 'openlucius_events_extras') . '/css/openlucius_events_extras.css');
  }
}

/**
 * Implements hook_permission().
 */
function openlucius_events_extras_permission() {
  return array(
    'unflag attending'                          => array(
      'title'       => t('Be able to unflag attending events'),
      'description' => t('Permission to check whether the user can unflag attending'),
    ),
    'unflag not attending'                      => array(
      'title'       => t('Be able to unflag not attending events'),
      'description' => t('Permission to check whether the user can unflag not attending'),
    ),
    'administer openlucius event configuration' => array(
      'title'       => t('Offer access to OpenLucius event configuration'),
      'description' => t('Permission to check whether the user can access the OpenLucius event configuration'),
    ),
    'send openlucius event reminders'           => array(
      'title'       => t('Offer access to send OpenLucius event reminders'),
      'description' => t('Permission to check whether the user can send the OpenLucius event reminders'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function openlucius_events_extras_menu() {
  $items = array();

  $items['admin/config/openlucius/events'] = array(
    'title'            => 'Event settings',
    'description'      => 'Configure event settings.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('openlucius_events_extras_settings_form'),
    'access arguments' => array('administer openlucius event configuration'),
    'type'             => MENU_LOCAL_TASK,
    'tab_parent'       => 'admin/config/openlucius',
    'weight'           => 2,
  );
  $items['unsubscribe']                    = array(
    'title'            => 'Unsubscribe user',
    'description'      => 'Unsubscribe user from event',
    'page callback'    => 'openlucius_events_extras_unsubscribe',
    'access arguments' => array('administer openlucius event configuration'),
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function openlucius_events_extras_block_info() {
  return array(
    'openlucius_events_flag'     => array(
      'info' => t('Event flag'),
    ),
    'openlucius_events_location' => array(
      'info' => t('Openlucius maps'),
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function openlucius_events_extras_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'openlucius_events_flag':
      $block['subject'] = NULL;
      $block['content'] = openlucius_events_extras_get_event_flag_html();
      break;

    case 'openlucius_events_location':
      $node             = menu_get_object();
      $block['subject'] = NULL;
      $content          = '';

      // Add content if location field not empty.
      if (!empty($node->field_event_location)) {
        $content = '<div id="map-canvas"></div>';
        $content .= '<input type="hidden" value="' . $node->field_event_location[LANGUAGE_NONE][0]['value'] . '" id="map-location" />';
      }
      $block['content'] = $content;
      break;
  }

  return $block;
}

/**
 * Implements hook_preprocess().
 */
function openlucius_events_extras_preprocess_node(&$variables) {
  if ($variables['type'] == 'ol_event') {
    if (!empty($variables['field_event_location'])) {

      // Add the google maps js.
      drupal_add_js('https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true', 'external');
      drupal_add_js(drupal_get_path('module', 'openlucius_events_extras') . '/js/openlucius_events_extras.js');
    }
  }
}

/**
 * All custom functions below this line.
 */

/**
 * Callback for getting the event flag for this event.
 * @return bool
 *   Returns the html for the flag if there is a valid node id.
 */
function openlucius_events_extras_get_event_flag_html() {
  global $user;

  $node = menu_get_object();
  $html = '';

  // Check if there's a node id.
  if (isset($node->nid)) {

    $flag_attending     = flag_get_flag('attending_event')->name;
    $flag_not_attending = flag_get_flag('not_attending_event')->name;

    // Check the status for both flags.
    $is_attending     = openlucius_events_extras_flag_check_status($flag_attending, $node->nid, $user->uid);
    $is_not_attending = openlucius_events_extras_flag_check_status($flag_not_attending, $node->nid, $user->uid);

    $attending_flag     = flag_create_link($flag_attending, $node->nid);
    $not_attending_flag = flag_create_link($flag_not_attending, $node->nid);

    // Check if the user has access to send reminder emails.
    if (user_access('send openlucius event reminders')) {
      $form = drupal_get_form('openlucius_events_extras_reminder_form');

      // Prepend the send reminders button.
      $html .= '<div class="notify-button">';
      $html .= drupal_render($form);
      $html .= '</div>';
    }

    // Check if the user is already attending or not attending.
    if (!$is_attending && !$is_not_attending) {
      $html .= '<div class="event-buttons">' . $attending_flag . $not_attending_flag . '</div>';
    }
    else {
      if ($is_attending) {
        if (user_access('unflag attending', $user)) {
          $html .= $attending_flag;
        }
      }

      if ($is_not_attending) {
        if (user_access('unflag not attending', $user)) {
          $html .= $not_attending_flag;
        }
      }
    }

    // Check if there are people attending.
    $html .= '<h2 class="block-title">' . t('Attending') . ' (' . openlucius_events_extras_flag_get_count($flag_attending, $node->nid) . ')</h2>';
    $html .= openlucius_events_extras_flag_get_people($flag_attending, $node->nid, 'list');

    // Check if there are people not attending.
    $html .= '<h2 class="block-title">' . t('Not attending') . ' (' . openlucius_events_extras_flag_get_count($flag_not_attending, $node->nid) . ')</h2>';
    $html .= openlucius_events_extras_flag_get_people($flag_not_attending, $node->nid, 'list');
  }

  drupal_alter('openlucius_core_event_flag', $html);

  return $html;
}

/**
 * Function to check whether a user has flagged a specific flag.
 *
 * @param string $flag
 *   The name of the flag (i.e. 'attending_flag').
 * @param int $nid
 *   The node id the flag is on.
 * @param int $uid
 *   The user to check has flagged.
 *
 * @return bool
 *   Returns true if the user has flagged the flag.
 */
function openlucius_events_extras_flag_check_status($flag, $nid, $uid) {
  $flag = flag_get_flag($flag);
  return $flag->is_flagged($nid, $uid);
}

/**
 * Function to get the flag count.
 *
 * @param string $name
 *   The name of the flag.
 * @param int $nid
 *   The node id for the flag count.
 *
 * @return bool
 *   Returns the flag count if TRUE.
 */
function openlucius_events_extras_flag_get_count($name, $nid) {

  // Get the flag.
  $flag = flag_get_flag($name);

  // Check the flag.
  if ($flag) {

    // Return the flag count for this nid.
    return $flag->get_count($nid);
  }

  return FALSE;
}

/**
 * Callback for getting the attendees for this event.
 *
 * @param string $flag
 *   The flag to select the people for.
 * @param int $nid
 *   The node id of the event to check.
 * @param string $type
 *   Whether to return as themed item list or regular array.
 *
 * @return mixed
 *   Returns the people that have flagged the selected flag
 */
function openlucius_events_extras_flag_get_people($flag, $nid, $type) {

  // Check if there is a nid supplied.
  if (isset($nid)) {
    $html          = '';
    $not_attending = 'not_attending_event';
    $attending     = 'attending_event';

    if ($flag == $not_attending) {
      $count = flag_get_entity_flags('flagging', $nid, $not_attending);

      // Get all reasons.
      $query = db_select('field_data_field_event_reason', 'fr')
        ->fields('f', array('uid'))
        ->fields('fr', array('field_event_reason_value'))
        ->condition('fr.bundle', 'not_attending_event', '=')
        ->condition('f.entity_id', $nid, '=');

      $query->leftJoin('flagging', 'f', 'fr.entity_id = f.flagging_id');
      $result = $query->execute()->fetchAllKeyed(0, 1);
    }
    elseif ($flag == $attending) {
      $count = flag_get_entity_flags('flagging', $nid, $attending);
    }

    if (isset($count)) {
      // Generate query.
      $destination          = drupal_get_destination();
      $destination['token'] = drupal_get_token();

      foreach ($count as $data) {
        $account  = entity_load('user', array($data->uid));
        $username = !empty($account[$data->uid]->realname) ? $account[$data->uid]->realname : $account[$data->uid]->name;
        $title    = !empty($result[$data->uid]) ? $result[$data->uid] : '';

        if ($flag == $not_attending) {
          $remove            = l(t('x'), 'unsubscribe/' . 'not_attending_event/' . $nid . '/' . $data->uid, array('query' => $destination));
          $items[$data->uid] = '<span class="reason" data-toggle="tooltip" data-original-title="' . $title . '">' . $username . ' ( ' . $remove . ' )</span>';
        }
        elseif ($flag == $attending) {
          $remove            = l(t('x'), 'unsubscribe/' . 'attending_event/' . $nid . '/' . $data->uid, array('query' => $destination));
          $items[$data->uid] = $username . ' ( ' . $remove . ' )';
        }
      }

      // Check if there are people attending the event.
      if (isset($items)) {

        // Check for the type to return.
        if ($type == 'list') {
          $html .= '<div class="list">' . theme('item_list', array('items' => $items)) . '</div>';
        }
        elseif ($type == 'plain') {
          $html .= $items;
        }
      }
    }

    // Return the html.
    return $html;
  }

  return FALSE;
}

/**
 * Form constructor for the reminder form.
 * @see openlucius_events_extras_reminder_form_submit()
 * @ingroup forms
 */
function openlucius_events_extras_reminder_form($form, &$form_state) {
  $form = array();

  $item      = menu_get_item();
  $group_nid = 0;

  if (isset($item['page_arguments'][0]->nid)) {
    $nid = $item['page_arguments'][0]->nid;

    // Check if there's a group reference id.
    if (isset($item['page_arguments'][0]->field_shared_group_reference[LANGUAGE_NONE][0]['nid'])) {
      $group_nid = $item['page_arguments'][0]->field_shared_group_reference[LANGUAGE_NONE][0]['nid'];
    }
  }

  // Return empty array.
  else {
    return array();
  }

  $form['send'] = array(
    '#type'       => 'submit',
    '#value'      => t('Send reminders'),
    '#attributes' => array(
      'class' => array(
        'btn',
        'btn-sm',
        'btn-warning',
        'btn-block',
      ),
    ),
  );

  // Set the event nid and the group nid.
  $form['nid']       = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['group_nid'] = array(
    '#type'  => 'hidden',
    '#value' => $group_nid,
  );

  return $form;
}

/**
 * Form submission handler for openlucius_core_add_todo_form().
 */
function openlucius_events_extras_reminder_form_submit($form, &$form_state) {

  // Get the node id the flag is on.
  $nid   = $form_state['values']['nid'];
  $group = $form_state['values']['group_nid'];

  // Load all the users.
  if ($group !== 0) {
    $users = openlucius_core_fetch_users('group', $group);
  }
  else {
    $users = entity_load('user');
  }

  // Get all users who have flagged this node.
  $flagged_users = db_select('flagging', 'f')
    ->fields('f', array('uid'))
    ->condition('f.entity_id', $nid, '=')
    ->execute()
    ->fetchAllKeyed(0, 0);

  // Get diff between user arrays.
  $remaining = array_diff(array_keys($users), array_keys($flagged_users));
  $targets   = array();
  foreach ($remaining as $uid) {
    $targets[] = $users[$uid]->mail;
  }

  openlucius_events_extras_send_reminder_mail($targets, $nid);
}

/**
 * Function to send reminder emails.
 *
 * @param array $targets
 *   The users to be notified.
 * @param int $nid
 *   The node for which the reminders need to be sent.
 */
function openlucius_events_extras_send_reminder_mail($targets, $nid) {

  // Setting the email parameters.
  $subject    = variable_get('openlucius_events_extras_reminder_title', '');
  $body_value = variable_get('openlucius_events_extras_reminder_text', '');
  $body       = isset($body_value['value']) ? $body_value['value'] : $body_value;

  $notification = new openlucius_Notification();
  $notification->setHeading('Openlucius');
  $notification->setReadMore('node/' . $nid);
  $notification->setTitle($subject);
  $notification->setBody($body);
  $notification->setTargets($targets);
  $notification->send();
}

/**
 * Form constructor for the event settings form.
 * @ingroup forms
 */
function openlucius_events_extras_settings_form($form, &$form_state) {

  $form = array();
  global $user;

  // Event reminder email title.
  $form['openlucius_events_extras_reminder_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Event reminder email title'),
    '#default_value' => variable_get('openlucius_events_extras_reminder_title', ''),
  );

  $value                                          = variable_get('openlucius_events_extras_reminder_text', '');
  $form['openlucius_events_extras_reminder_text'] = array(
    '#title'         => t('Event reminder email text'),
    '#type'          => 'text_format',
    '#rows'          => 20,
    '#format'        => filter_default_format($user),
    '#default_value' => is_array($value) && array_key_exists('value', $value) ? $value['value'] : $value,
  );

  return system_settings_form($form);
}

/**
 * Implements openlucius_events_extras_preprocess_flag().
 */
function openlucius_events_extras_preprocess_flag(&$vars) {
  if ($vars['flag']->name == 'attending_event') {
    $vars['flag_classes_array'][] = 'btn';
    $vars['flag_classes_array'][] = 'btn-success';
    $vars['flag_classes_array'][] = 'btn-sm';
  }
  elseif ($vars['flag']->name == 'not_attending_event') {
    $vars['flag_classes_array'][] = 'btn';
    $vars['flag_classes_array'][] = 'btn-danger';
    $vars['flag_classes_array'][] = 'btn-sm';
  }
}

/**
 * Callback for unsubscribing users from events.
 */
function openlucius_events_extras_unsubscribe() {

  if (isset($_GET['token']) && drupal_valid_token($_GET['token'])) {

    // Get the menu item.
    $object = menu_get_item();

    // Check if the node id and user id are set.
    if (isset($object['map'][1]) && isset($object['map'][2]) && isset($object['map'][3])) {

      // The required values.
      $type = $object['map'][1];
      $nid  = $object['map'][2];
      $uid  = $object['map'][3];

      // Get the flag.
      $flag = flag_get_flag($type);

      // Unflag the user.
      $flag->flag('unflag', $nid, user_load($uid));
    }
  }

  // Return.
  drupal_goto();
}

/**
 * Implements hook_openlucius_core_config_places_alter().
 */
function openlucius_events_extras_openlucius_core_config_places_alter(&$places) {
  $places[] = 'admin/config/openlucius/events';
  $places[] = 'admin/config/openlucius/events-not-attending';
}
