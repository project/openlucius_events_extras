
-- SUMMARY --
This module offers extra functionality for Events in the OpenLucius distro:

Let users choose: ‘Attending’ or ‘Not attending’.
Send email reminders to all users that did not make a choice yet.
Let users provide a reason: ‘Why are you not attending?’
Show a Google map with location of Event.

-- REQUIREMENTS --

Openlucius https://www.drupal.org/project/openlucius

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure module at YOURSITE/admin/config/openlucius/events
